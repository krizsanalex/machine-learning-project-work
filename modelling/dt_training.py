import argparse
from joblib import dump
import pandas as pd
from sklearn import tree

def get_optimal_decision_tree_solution(data, label):
  # 'max_depth': 10, 'max_features': 5, 'min_samples_leaf': 1, 'min_samples_split': 10
  clf = tree.DecisionTreeClassifier(max_features=5, max_depth=10, min_samples_leaf=1, min_samples_split=10, random_state=9)
  clf.fit(data,label)
  return clf
  
  
# EXAMPLE: python dt_training.py --dataset "../data/processed_data_train.csv"  --max_depth 5 --max_features 10 --min_samples_split 10 --min_samples_leaf 1
if __name__ == "__main__":

    # -- Accepted command line arguments. --

    ARGUMENT_DATASET = '--dataset'
    ARGUMENT_MAX_DEPTH = '--max_depth'
    ARGUMENT_MAX_FEATURES = '--max_features'
    ARGUMENT_MIN_SAMPLES_SPLIT = '--min_samples_split'
    ARGUMENT_MIN_SAMPLES_LEAF = '--min_samples_leaf'

    # -- Parsing the command line arguments. --

    arg_parser = argparse.ArgumentParser()

    arg_parser.add_argument(ARGUMENT_DATASET, nargs=1, type=str)
    arg_parser.add_argument(ARGUMENT_MAX_DEPTH, nargs=1, type=int)
    arg_parser.add_argument(ARGUMENT_MAX_FEATURES, nargs=1, type=int)
    arg_parser.add_argument(ARGUMENT_MIN_SAMPLES_SPLIT, nargs=1, type=int)
    arg_parser.add_argument(ARGUMENT_MIN_SAMPLES_LEAF, nargs=1, type=int)

    args = arg_parser.parse_args()
    dataset = args.dataset[0]
    max_depth = args.max_depth[0]
    max_features = args.max_features[0]
    min_sample_split = args.min_samples_split[0]
    min_sample_leaf = args.min_samples_leaf[0]


    train = pd.read_csv(dataset)
    train_label = train['label']
    train_data = train.drop('label', axis=1)

    clf = tree.DecisionTreeClassifier(max_features=max_features, max_depth=max_depth, min_samples_leaf=min_sample_leaf, min_samples_split=min_sample_split, random_state=9)

    clf.fit(train_data,train_label)
    dump(clf, 'decision-tree.joblib')

