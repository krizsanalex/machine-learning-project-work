import argparse
import pandas as pd
from sklearn.metrics import accuracy_score, cohen_kappa_score
from baseline_train import get_baseline
from dt_training import get_optimal_decision_tree_solution
from nb_training import get_optimal_nb
from knn_training import get_optimal_knn


def evaluate_single_model(model, metric: str) -> float:
    """
    Evaluates a single model based on the given metric.

    :param model: a trained model.
    :param metric: a metric for scoring, default is cohen_kappa.

    :return: the score based on the model's predictions.
    """
    prediction = model.predict(test_features)

    if metric == CHOICE_ACCURACY:
        return accuracy_score(y_pred=prediction, y_true=test_labels)
    else:
        return cohen_kappa_score(prediction, test_labels, weights="quadratic")


def compare_models():
    """
    Evaluates all models on with all metrics and prints a formatted table.
    """
    models = {
        CHOICE_BASELINE: get_baseline(train_features, train_labels),
        CHOICE_DECISION_TREE: get_optimal_decision_tree_solution(train_features, train_labels),
        CHOICE_NAIVE_BAYES: get_optimal_nb(train_features, train_labels),
        CHOICE_KNN: get_optimal_knn(train_features, train_labels)
    }

    scores = dict()

    for key, model in models.items():
        scores[key] = (evaluate_single_model(model, CHOICE_ACCURACY), evaluate_single_model(model, CHOICE_COHEN_KAPPA))

    print('score', end=' '*10)
    for key in scores.keys():
        print(f'{key}', end=' '*(15-len(key)))
    print(f'\r\n{CHOICE_ACCURACY}', end=' '*(15-len(CHOICE_ACCURACY)))
    for value in scores.values():
        print(f'{value[0]:.8f}', end=' '*5)
    print(f'\r\n{CHOICE_COHEN_KAPPA}', end=' '*(15-len(CHOICE_COHEN_KAPPA)))
    for value in scores.values():
        print(f'{value[1]:.8f}', end=' '*5)


# EXAMPLE: python evaluate.py --model "baseline" --metric "accuracy"
if __name__ == "__main__":

    # -- Accepted command line arguments. --

    ARGUMENT_MODEL = '--model'
    ARGUMENT_METRIC = '--metric'
    ARGUMENT_COMPARE = '--compare'

    # -- Accepted model and metric types. --

    CHOICE_BASELINE = 'baseline'
    CHOICE_DECISION_TREE = 'decision_tree'
    CHOICE_NAIVE_BAYES = 'naive_bayes'
    CHOICE_KNN = 'knn'

    CHOICE_ACCURACY = 'accuracy'
    CHOICE_COHEN_KAPPA = 'cohen_kappa'

    MODEL_CHOICES = [CHOICE_BASELINE, CHOICE_DECISION_TREE, CHOICE_NAIVE_BAYES, CHOICE_KNN]
    METRIC_CHOICES = [CHOICE_ACCURACY, CHOICE_COHEN_KAPPA]

    # -- Parsing the command line arguments. --

    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument(ARGUMENT_MODEL, type=str, choices=MODEL_CHOICES, default=CHOICE_BASELINE)
    arg_parser.add_argument(ARGUMENT_METRIC, type=str, choices=METRIC_CHOICES, default=CHOICE_COHEN_KAPPA)
    arg_parser.add_argument(ARGUMENT_COMPARE, action='store_true')
    args = arg_parser.parse_args()

    # -- Reading train and test dataset --

    TRAIN_PATH = '../data/processed_data_train.csv'
    train = pd.read_csv(TRAIN_PATH)
    train_labels = train['label']
    train_features = train.drop('label', axis=1)

    TEST_PATH = '../data/processed_data_test.csv'
    test = pd.read_csv(TEST_PATH)
    test_labels = test['label']
    test_features = test.drop('label', axis=1)

    # -- Evaluate all or a specified model. --

    if args.compare:
        compare_models()
    else:
        if args.model == CHOICE_DECISION_TREE:
            score = evaluate_single_model(get_optimal_decision_tree_solution(train_features, train_labels), args.metric)
        elif args.model == CHOICE_NAIVE_BAYES:
            score = evaluate_single_model(get_optimal_nb(train_features, train_labels), args.metric)
        elif args.model == CHOICE_KNN:
            score = evaluate_single_model(get_optimal_knn(train_features, train_labels), args.metric)
        else:
            score = evaluate_single_model(get_baseline(train_features, train_labels), args.metric)

        print(f'{args.metric} of {args.model}: {score}')
