from sklearn.dummy import DummyClassifier


def get_baseline(features, labels):
    baseline_clf = DummyClassifier(strategy="most_frequent")
    baseline_clf.fit(features, labels)
    return baseline_clf
