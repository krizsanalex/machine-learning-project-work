import argparse
from joblib import dump
import pandas as pd
from sklearn.neighbors import KNeighborsClassifier

def get_optimal_knn(data, label):
  # 'n_neighbors': 24
  knn = KNeighborsClassifier(24)
  knn.fit(data,label)
  return knn
  
  
# EXAMPLE: python dt_training.py --dataset "../data/processed_data_train.csv"  --n_neighbors 24
if __name__ == "__main__":

    # -- Accepted command line arguments. --

    ARGUMENT_DATASET = '--dataset'
    ARGUMENT_N_NEIGHBORS = '--n_neighbors'

    # -- Parsing the command line arguments. --

    arg_parser = argparse.ArgumentParser()

    arg_parser.add_argument(ARGUMENT_DATASET, nargs=1, type=str)
    arg_parser.add_argument(ARGUMENT_N_NEIGHBORS, nargs=1, type=int)

    args = arg_parser.parse_args()
    dataset = args.dataset[0]
    n_neighbors = args.n_neighbors[0]

    train = pd.read_csv(dataset)
    train_label = train['label']
    train_data = train.drop('label', axis=1)

    knn = KNeighborsClassifier(n_neighbors=n_neighbors)

    knn.fit(train_data,train_label)
    dump(knn, 'knn.joblib')

