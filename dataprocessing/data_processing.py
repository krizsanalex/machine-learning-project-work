import sys
import argparse
import pandas as pd
from sklearn import preprocessing
from sklearn.decomposition import PCA 
from sklearn.model_selection import train_test_split

# -- Accepted command line arguments. --

ARGUMENT_TRAIN_RATIO = '--train_ratio'
ARGUMENT_JOIN_METHOD = '--join'
ARGUMENT_N_COMPONENTS = '--n_components'
ARGUMENT_DEGREE = '--degree'
ARGUMENT_FEATURE_BASE = '--feature_base'

# -- File paths. --

PATH_TRAIN_CSV = '../data/train.csv'
PATH_TRAIN_METADATA_CSV = '../data/train_metadata_features.csv'
PATH_TRAIN_SENTIMENT_CSV = '../data/train_sentiment_features.csv'
PATH_TRAIN_BIN_CSV = '../data/combination_features.csv'

# -- Parsing the command line arguments. --

arg_parser = argparse.ArgumentParser(description='Running customizable data processing on the inital data.')

arg_parser.add_argument(ARGUMENT_TRAIN_RATIO, nargs=1, default=[0.8], type=float, metavar='ratio', dest='train_split_ratio', help='The percentage of the size of the train dataset. Define the value in the [0, 1] interval!')

arg_parser.add_argument(ARGUMENT_JOIN_METHOD, nargs=1, default=['left'], type=str, metavar='[left, inner]', dest='join_method', choices=['left', 'inner'], help='Left join preserve the original dataset while adding the addtional data, while inner join will result in only those rows, which had matching values on both datasets.')

arg_parser.add_argument(ARGUMENT_N_COMPONENTS, nargs=1, default=[10], type=int, metavar='N', dest='n_components', help='The number of features to keep, which describes the dataset best.')

arg_parser.add_argument(ARGUMENT_DEGREE, nargs=1, default=[2], type=int, metavar='D', dest='degree', help='The polynimial degree used in PolyFeatures.')

arg_parser.add_argument(ARGUMENT_FEATURE_BASE, nargs='+', default=None, type=str, metavar='f', dest='feature_base', help='The column names which the feature extraction base. If no specified all features will be used.')

args = arg_parser.parse_args()

train_split_ratio = args.train_split_ratio[0]
if train_split_ratio < 0.0 or train_split_ratio > 1.0:
    print('The train_split_ratio must be in the interval: [0,1]!')
    exit()
join_method = args.join_method[0]
n_components = args.n_components[0]
degree = args.degree[0]
feature_base = args.feature_base
if feature_base == None:
    feature_base_str = 'Not used.' 
else: 
    feature_base_str = str(feature_base)

print('The script running with the following options:\r\n\
    train split ratio: {}\r\n\
    join method      : {}\r\n\
    n components     : {}\r\n\
    degree           : {}\r\n\
    feature base     : {}\r\n\
    \r\n'.format(train_split_ratio, join_method, n_components, degree, feature_base_str))
    
# -- Loading train.csv --

df_base = pd.read_csv(PATH_TRAIN_CSV)
print('Base dataframe shape: ' + str(df_base.shape))

# -- Loading the sentiment data. --

# More about the sentiment data see 'sentiment_features_extraction.ipynb'.

df_sentiment = pd.read_csv(PATH_TRAIN_SENTIMENT_CSV)
print('Sentiment data dataframe shape: ' + str(df_sentiment.shape))

# CHOOSING JOIN METHOD:
# Using left join will preserve all the entries from the left side (df_base) dataframe, while adding the additional coloums from the right side data.
# Inner join will return only those rows which had matching values on both dataframes.
# The dataframes are joined by the 'PetID' coloums.

df_extended = pd.merge(left=df_base, right=df_sentiment, how=join_method, left_on='PetID', right_on='PetID')

print('Dataframe after sentiment data is added: ' + str(df_extended.shape))

# -- Loading the image metadata. --

# More about the image metadata see 'metadata_feature_extraction.ipynb'.

df_metadata = pd.read_csv(PATH_TRAIN_METADATA_CSV)
print('Image metadata dataframe shape: ' + str(df_metadata.shape))

# CHOOSING JOIN METHOD:
# Using left join will preserve all the entries from the left side (df_base) dataframe, while adding the additional coloums from the right side data.
# Inner join will return only those rows which had matching values on both dataframes.
# The dataframes are joined by the 'PetID' coloums.

df_extended = pd.merge(left=df_extended, right=df_metadata, how=join_method, left_on='PetID', right_on='PetID')

print('Dataframe after image metadata is added: ' + str(df_extended.shape))


# -- Feature binarisation. --

df_comb = pd.read_csv(PATH_TRAIN_BIN_CSV)
print('Combination feature dataframe shape: ' + str(df_comb.shape))

# CHOOSING JOIN METHOD:
# Using left join will preserve all the entries from the left side (df_base) dataframe, while adding the additional coloums from the right side data.
# Inner join will return only those rows which had matching values on both dataframes.
# The dataframes are joined by the 'PetID' coloums.

df_extended = pd.merge(left=df_extended, right=df_comb, how=join_method, left_on='PetID', right_on='PetID')

print('Dataframe after combination features are added: ' + str(df_extended.shape))


# -- Feature discretisation. --

df_full = df_extended
data_filtered = df_full.drop(["Name", "RescuerID", "PetID", "Description"], axis = 1)
print('Dataframe after filtering: ' + str(data_filtered.shape))

# For explanation about the method used for discretising and separator values see 'Discretization_of_continuous_features.ipynb'.

data_filtered.loc[data_filtered["Age"] <= 3, "Age"] = 0
data_filtered.loc[(data_filtered["Age"] > 3) & (data_filtered["Age"] <= 12), "Age"] = 1
data_filtered.loc[data_filtered["Age"] > 12, "Age"] = 2

data_filtered.loc[data_filtered["Quantity"] <= 1, "Quantity"] = 0
data_filtered.loc[data_filtered["Quantity"] > 1, "Quantity"] = 1

data_filtered.loc[data_filtered["Fee"] == 0 , "Fee"] = 0
data_filtered.loc[data_filtered["Fee"] > 0, "Fee"] = 1

data_filtered.loc[data_filtered["PhotoAmt"] <= 3 , "PhotoAmt"] = 0
data_filtered.loc[data_filtered["PhotoAmt"] > 3, "PhotoAmt"] = 1

data_filtered.loc[data_filtered["VideoAmt"] == 0 , "VideoAmt"] = 0
data_filtered.loc[data_filtered["VideoAmt"] > 0, "VideoAmt"] = 1


# -- Feature and label extraction from the pandas dataframe. --

# Dropping NaN values.

data_filtered.dropna(inplace=True)
data_filtered.reset_index(drop=True, inplace=True)
print('NaN values are dropped. ' + str(data_filtered.shape))

# Encode state.

ordinal_encoder = preprocessing.OrdinalEncoder()
state_encoded = ordinal_encoder.fit_transform(data_filtered["State"].values.reshape(-1, 1)) 
data_filtered["State"] = state_encoded.flatten()

# Separate features and labels.

labels = data_filtered["AdoptionSpeed"]
features = data_filtered.copy().drop("AdoptionSpeed", axis = 1)


# For more about feature selection on this dataset see 'polyfeatures_and_pca.ipynb'.

def poly_features(dataframe: pd.DataFrame, degree: int, on_features: list) -> pd.DataFrame:

    input_features = dataframe[on_features].copy()
    dataframe = dataframe.copy().drop(on_features, axis = 1)

    poly = preprocessing.PolynomialFeatures(degree)
    output_features = pd.DataFrame(data=poly.fit_transform(input_features))
    dataframe = pd.concat([dataframe, output_features], axis = 1)

    return dataframe

def pca(dataframe: pd.DataFrame, n_components: int) -> pd.DataFrame:
    dataframe =  preprocessing.StandardScaler().fit_transform(dataframe)
    pca = PCA(n_components=n_components)
    principalComponents = pca.fit_transform(dataframe)
    return pd.DataFrame(data = principalComponents)

if feature_base is not None:
    for f in feature_base:
        if f not in features.columns:
            print('The given feature \'{}\' is not the dataframe.'.format(f))
            exit()
            
    features = poly_features(features, degree, feature_base)
    print('PolyFeatures added: ' + str(features.shape))
    
filtered_features = pca(features, n_components)
print('PCA with {} n_components: '.format(n_components) + str(filtered_features.shape))

# -- Splitting the data to train and test sets. --

train_features, test_features, train_labels, test_labels = train_test_split(filtered_features, labels, test_size=1.0-train_split_ratio)


# -- Generating output csv files. --

train_data = train_features.assign(label = train_labels)
test_data = test_features.assign(label = test_labels)

train_data.to_csv("processed_data_train.csv", index=False)
test_data.to_csv("processed_data_test.csv", index=False)



